/*jshint node:true*/
var path = require('path'),
    express = require('express'),
    httpProxy  = require('http-proxy'),
    proxy = httpProxy.createProxyServer(),
    app = express();

app.use(express.static(__dirname + '/public'));

app.all('/api/*', function (req, res) {
    req.headers.host = 'zadane.pl';
    req.url = req.url.replace('/api/', '');
    proxy.web(req, res, {target: 'http://zadane.pl/api/23'});
});

var server = app.listen(3000, function () {
    var host = server.address().address,
    port = server.address().port;

    console.log('Server listening at http://%s:%s', host, port);
});