require.config({
    paths: {
        jquery: 'libs/jQuery/dist/jquery',
        underscore: 'libs/underscore/underscore',
        backbone: 'libs/backbone/backbone',
        text: "libs/requirejs-text/text",
        templates: "../templates",
        app: 'application'
    }
});
require(['app'], function(app){
    app.initialize();
});