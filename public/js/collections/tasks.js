define(['underscore', 'backbone', 'config', 'models/task'], function(_, Backbone, config, Task){
    var TasksCollection = Backbone.Collection.extend({
        model: Task,
        hasMoreToFetch: true,
        newTasksCount: 0,
        url: function() {
            return config.apiUrl + '/api_tasks/index';
        },
        /**
         * Fetches next elements after fetch
         * @returns {Deferred}
         */
        fetchNext: function() {
            /*jshint camelcase: false */// - disabled option as we can't change API format

            // Check if we can fetch more elements
            if(!this.hasMoreToFetch) {
                return null;
            }
            else {
                // Call fetch with params
                return this.fetch({
                    forceMethod: 'create', // We need to use POST instead of GET for data fetching
                    remove: false, // We want to append elements into collection
                    data: {
                        last_id: this.last().get("id") // last_id is required by API calls
                    },
                    emulateJSON: true // Setting POST format
                });
            }
        },
        /**
         * Override Backbone.Collection.sync to support
         * forceMethod param in options
         *
         * @param method
         * @param model
         * @param options
         * @returns {Deferred}
         */
        sync: function(method, model, options) {
            /*jshint camelcase: false */// - disabled option as we can't change API format
            options = options || {};

            // Check if we need to override sync method
            method = options.forceMethod || method;

            return Backbone.Collection.prototype.sync.call(this, method, model, options);
        },
        /**
         * Parse data from API to custom structure
         *
         * @param response
         * @returns {Array}
         */
        parse: function(response) {
            /*jshint camelcase: false */// - disabled option as we can't change API format

            // Get tasks list
            var tasks = response && response.data && response.data.tasks && response.data.tasks.items;
            if(tasks) {
                // Lets put info on collection if there are more task to be fetched
                if(tasks.length === 0) {
                    this.hasMoreToFetch = false;
                }
                // Add info about how many new task we got
                this.newTasksCount = tasks.length;

                // Update tasks structure from API to create Task model instances
                tasks = _.map(tasks, function(element) {
                    // Nest presence data
                    element.task.presence = element.presence;

                    // Add user data
                    element.task.user = _.find(response.users_data, {id: element.task.user_id});
                    return element.task;
                });
            }
            return tasks;
        }
    });
    return TasksCollection;
});