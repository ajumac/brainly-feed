define([
    'underscore',
    'backbone',
    'collections/tasks',
    'text!templates/tasks-list.html',
    'text!templates/tasks-list-element.html',
    'text!templates/loader.html'
], function(_, Backbone, TasksCollection, tasksListTemplate, tasksListElementTemplate, loaderTemplate) {
    var FeedView = Backbone.View.extend({
        el: '#content',
        events: {
            "click .tasks-list-more-button": "onTasksListMoreButtonClick"
        },
        tasksListTemplate: _.template(tasksListTemplate),
        tasksListElementTemplate: _.template(tasksListElementTemplate),
        loaderTemplate: _.template(loaderTemplate),
        /**
         * Contructor function
         */
        initialize: function() {
            this.collection = new TasksCollection();

            this.listenTo(this.collection, 'sync', this.renderTasks);
            this.listenTo(this.collection, 'error', this.onFetchDataError);

            this.collection.fetch();
        },
        /**
         * Render loader while fetching data
         */
        render: function() {
            this.$el.html(this.loaderTemplate());
        },
        /**
         * Render tasks data after sync operation
         */
        renderTasks: function() {
            var i,
                // Cache collection count
                tasksCount = this.collection.length,
                // Count how many tasks should be hidden
                hiddenTasks = tasksCount % 3,
                // Tasks data as string
                tasks = '';

            //Iterate from the last added tasks
            for(i = tasksCount - this.collection.newTasksCount; i < tasksCount; i++) {
                var hiddenTask =  tasksCount - i <= hiddenTasks;
                tasks +=  this.tasksListElementTemplate({
                    hiddenTask: hiddenTask,
                    task: this.collection.at(i)
                });
            }

            // If there is tasks list
            if(this.tasksListDOM) {
                // Show previously hidden tasks
                this.tasksListDOM.find('.task-hidden').removeClass('task-hidden');

                // Append new tasks to existing list
                this.tasksListDOM.append(tasks);
            }
            else {
                // Else if there is no task list create one
                this.$el.html(this.tasksListTemplate({tasks: tasks}));

                // Cache DOM elements
                this.tasksListDOM = this.$el.find('.tasks-list');
                this.tasksListMoreButtonDOM = this.$el.find('.tasks-list-more-button');
            }

            // Handle more button status
            if(this.collection.hasMoreToFetch) {
                this.tasksListMoreButtonDOM.removeAttr('disabled');
            }
            else {
                this.tasksListMoreButtonDOM.remove();
            }
        },
        /**
         * Handle more button click
         */
        onTasksListMoreButtonClick: function() {
            this.collection.fetchNext();
            this.tasksListMoreButtonDOM.attr("disabled", "disabled");
        },
        /**
         * Handle data fetch errors
         */
        onFetchDataError: function() {
            window.alert('Nie udało się pobrać danych.');
            if(this.tasksListMoreButtonDOM) {
                this.tasksListMoreButtonDOM.removeAttr('disabled');
            }
        }
    });
    return FeedView;
});