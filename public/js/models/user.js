define(['config', 'backbone'], function(config, Backbone){
    var User = Backbone.Model.extend({
        /**
         * Get user avatar URL
         *
         * @returns {string}
         */
        getAvatar: function() {
            var avatar;
            if(this.get("avatars") && this.get("avatars")["64"]) {
                avatar = this.get("avatars")["64"];
            }
            else {
                if(this.get("gender") === 2) {
                    avatar = config.avatarMale;
                }
                else {
                    avatar = config.avatarFemale;
                }
            }
            return avatar;
        },
        /**
         * Get user profile page URL
         *
         * @returns {string}
         */
        getProfileUrl: function(){
            return config.pageUrl + 'profil/' + this.get("nick") + '-' + this.get("id");
        }
    });
    return User;
});