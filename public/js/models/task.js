define(['backbone', 'config', 'models/user'], function(Backbone, config, User){
    var Task = Backbone.Model.extend({
        /**
         * Parse Task data
         *
         * @param data
         * @returns {*}
         */
        parse: function(data) {
            if(data.user) {
                data.user = new User(data.user);
            }
            return data;
        },
        /**
         * Get page url for single task
         *
         * @returns {string}
         */
        getUrl: function() {
            return config.pageUrl + 'zadanie/' + this.get("id");
        }

    });
    return Task;
});