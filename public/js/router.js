define(['backbone', 'views/feed'], function(Backbone, FeedView){
    var Router = Backbone.Router.extend({
        routes: {
            "": "home"
        },
        home: function(){
            this.currentView = new FeedView();
            this.currentView.render();
        }
    });
    return Router;
});