define(['backbone', 'router'], function(Backbone, Router){
    var app = {
        initialize: function(){
            this.router = new Router();
            Backbone.history.start();
        }
    };
    return app;
});