/*jshint node:true*/
var gulp = require('gulp'),
    jshint = require('gulp-jshint'),
    stylish = require('jshint-stylish'),
    spawn = require('child_process').spawn ;


gulp.task('watch', function(cb) {
    var sass = spawn('sass.bat', ['--watch', 'public/sass:public/css']);
    sass.stdout.on('data', function (data) {
        console.log(data.toString());
    });

    sass.stderr.on('data', function (data) {
        console.log(data.toString());
    });
});

gulp.task('lint', function() {
    return gulp.src('./public/js/**/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter(stylish));
});